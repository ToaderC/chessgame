﻿using ChessGame.BL.Interfaces;
using ChessGame.BL.Piece;

namespace ChessGame
{
    public class BoardCell : IBoardCell
    {
        public BoardCell()
        {}
        public BoardCell(IPiece piece)
        {
            Piece = piece;
        }

        public IPiece? Piece { get; set; }
        public bool HasPiece() => Piece != null;
    }
}
