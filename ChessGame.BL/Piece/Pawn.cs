﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGame.BL.Piece
{
    public class Pawn : PieceBase
    {
        public Pawn()
        {
        }

        public override IEnumerable<IMove> MoveTypes()
        {
            return new List<IMove>()
            {
                new UpMove(),
                new UpRightMove(),
            };
        }
    }
}
