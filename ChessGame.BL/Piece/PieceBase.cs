﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGame.BL.Piece
{
    public abstract class PieceBase : IPiece
    {
        public PieceColor Color { get; set; }

        public abstract IEnumerable<IMove> MoveTypes();
    }
}
