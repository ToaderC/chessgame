﻿using ChessGame.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGame.BL.Piece
{
    public interface IPiece : IMovable
    {
        PieceColor Color { get; set; }
    }

    public interface IMovable
    {
        IEnumerable<IMove> MoveTypes();
    }

    public interface IMoveType
    {
        Position NextCell(Position position);
    }

    public interface IChessRules
    {
        bool ValidateRule();
    }

    public interface IMove
    {
        Position NextCell(Position fromPosition);
        Position NextCell(Position formPosition, int distance);
        IEnumerable<Position> GetAllPossiblePositions(Position fromPosition, int boardSize);
    }

    public class UpMove : IMove
    {
        public Position NextCell(Position fromPosition)
        {
            return NextCell(fromPosition, 1);
        }

        public Position NextCell(Position fromPosition, int distance)
        {
            return new Position(fromPosition.X, fromPosition.Y + distance);
        }

        public IEnumerable<Position> GetAllPossiblePositions(Position fromPosition, int boardSize)
        {
            List<Position> result = new List<Position>();

            for (int i = fromPosition.Y; i < boardSize; i++)
            {
                result.Add(new Position(fromPosition.X, fromPosition.Y + 1));
            }

            return result;
        }
    }

    public class PawnCaptureMove : IMove
    {
        public IEnumerable<Position> GetAllPossiblePositions(Position fromPosition, int boardSize)
        {
            throw new NotImplementedException();
        }

        public Position NextCell(Position fromPosition)
        {
            throw new NotImplementedException();
        }

        public Position NextCell(Position formPosition, int distance)
        {
            throw new NotImplementedException();
        }
    }

    public class UpRightMove : IMove
    {
        public IEnumerable<Position> GetAllPossiblePositions(Position fromPosition, int boardSize)
        {
            throw new NotImplementedException();
        }

        public Position NextCell(Position fromPosition)
        {
            throw new NotImplementedException();
        }

        public Position NextCell(Position formPosition, int distance)
        {
            throw new NotImplementedException();
        }
    }

    // TODO: This should be good, extrant in new classes
    public interface IMoveProvider
    {
        Position NextCell(Position fromPosition);
    }

    public class UpMoveProvider : IMoveProvider
    {
        public Position NextCell(Position fromPosition)
        {
            return new Position(fromPosition.X, fromPosition.Y - 1);
        }
    }

    public class UpRightMoveProvider : IMoveProvider
    {
        Position IMoveProvider.NextCell(Position fromPosition)
        {
            return new Position(fromPosition.X + 1, fromPosition.Y - 1);
        }
    }

    public interface IMoveTemp
    {
        IEnumerable<Position> GetAllPossiblePositions(Position fromPosition, ChessBoard board);
    }

    public class UpRight : IMoveTemp
    {
        public IEnumerable<Position> GetAllPossiblePositions(Position fromPosition, ChessBoard board)
        {
            List<Position> result = new List<Position>();
            Position currentPosition = new Position(fromPosition);
            IMoveProvider moveProvider = new UpRightMoveProvider();

            while (BoardPosition.IsPositionInBoardBounds(currentPosition, board.Size()))
            {
                currentPosition = moveProvider.NextCell(currentPosition);
                result.Add(currentPosition);

                if (!board.IsEmptyPosition(currentPosition))
                {
                    break;
                }
            }

            return result;
        }
    }

    // TODO:
    // Ar trebui o interfata IMoveProvider sau ceva de genul care sa intorca mutari gen inainte, diagonala si asa si exact cum e IMove acum,
    // Apoi fiecare piesa ar trebuie sa intoarca o lista de IMove care sa fie concret ce mutari stie sa faca piesa in sine, de exemplu PawnCaptureMove care face doar o mutare pe diagonala,
    // Deci IMoveProvider ar trebuie sa stie sa-mi intoarca doar NextCell si atat, fara GetAllPossiblePositions
}
