﻿namespace ChessGame.BL.Exceptions
{
    public class NullChessComponentException : Exception
    {
        public NullChessComponentException(Type objectType)
            : base($"Uninitialized component {objectType}.")
        {}
    }
}
