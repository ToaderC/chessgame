﻿namespace ChessGame.BL.Exceptions
{
    public class PositionOutOfBoardException : Exception
    {
        public PositionOutOfBoardException(string message)
            : base(message)
        {}
    }
}
