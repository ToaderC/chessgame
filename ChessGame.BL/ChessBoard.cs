﻿using ChessGame.Common;
using ChessGame.BL.Interfaces;
using ChessGame.BL.Piece;

namespace ChessGame
{
    public class ChessBoard : IChessBoard
    {
        private readonly IBoardStorage _boardStorage;

        public ChessBoard(IBoardStorage boardStorage)
        {
            _boardStorage = boardStorage;
        }

        // TODO: AddPiece can add null Piece. In the future check if is ok like this
        // TODO: Should also perform checks at this level and don't rely on dependencies to do the checks
        // At this level can't know if the dependency will perform checks

        public bool AddPiece(IPiece piece, Position position)
        {
            var cell = _boardStorage.Get(position);
            if(cell.HasPiece())
            {
                return false;
            }
            cell.Piece = piece;

            return true;
        }

        public bool RemovePiece(Position position)
        {
            var cell = _boardStorage.Get(position);
            if(!cell.HasPiece())
            {
                return false;
            }
            cell.Piece = null;

            return true;
        }

        public bool IsEmptyPosition(Position position)
        {
            return _boardStorage.Get(position).HasPiece();
        }

        // TODO: Might need to work only with IPiece and outside don't care about the BoardCell
        public IBoardCell GetBoardCell(Position position)
        {
            return _boardStorage.Get(position);
        }

        public int Size()
        {
            return _boardStorage.GetSize();
        }
    }
}
