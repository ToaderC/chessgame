﻿using ChessGame.BL.Exceptions;
using ChessGame.BL.Interfaces;
using ChessGame.Common;

namespace ChessGame
{
    public class BoardStorage : IBoardStorage
    {
        private int _boardSize;

        private readonly IBoardCell[,] _boardCells;

        public BoardStorage(int boardSize)
        {
            _boardSize = boardSize;
            _boardCells = new IBoardCell[boardSize, boardSize];
        }

        public IBoardCell Get(Position position)
        {
            if(!IsPositionInBounds(position))
            {
                throw new PositionOutOfBoardException($"{typeof(Position)} is out of board bounds");
            }
            IBoardCell cell = _boardCells[position.X, position.Y];

            return cell ?? throw new NullChessComponentException(typeof(IBoardCell));
        }

        public void Set(IBoardCell cell, Position position)
        {
            if(!IsPositionInBounds(position))
            {
                throw new PositionOutOfBoardException($"{typeof(Position)} is out of board bounds");
            }

            _boardCells[position.X, position.Y] = cell;
        }

        public int GetSize()
        {
            return _boardSize;
        }

        // TODO: Maybe move this to BoardPosition if we already have a check for board there
        // TODO: Moved to BoardPosition, remove from here
        private bool IsPositionInBounds(Position position)
        {
            if (!BoardPosition.IsIndexInBoardBounds(position.X, _boardSize) || !BoardPosition.IsIndexInBoardBounds(position.Y, _boardSize))
            {
                return false;
            }

            return true;
        }

    }
}
