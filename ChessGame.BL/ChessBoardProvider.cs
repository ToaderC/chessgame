﻿using ChessGame.BL.Interfaces;
using ChessGame.BL.Piece;

namespace ChessGame.BL
{
    public class InitialBoardProvider : IChessBoardProvider
    {
        private IBoardStorage _boardStorage;

        public InitialBoardProvider()
        {
            _boardStorage = new BoardStorage(8);
        }
        public IChessBoard GetChessBoard()
        {
            SetPawns(PieceColor.White, _boardStorage);
            SetPawns(PieceColor.Black, _boardStorage);
            SetBlackFirstLine(_boardStorage);
            SetWhiteFirstLine(_boardStorage);

            return new ChessBoard(_boardStorage);
        }

        private void SetPawns(PieceColor color, IBoardStorage board)
        {
            switch (color)
            {
                case PieceColor.White:
                    break;
                case PieceColor.Black:
                    break;
            }
        }

        private void SetBlackFirstLine(IBoardStorage board)
        {

        }

        private void SetWhiteFirstLine(IBoardStorage board)
        {

        }
    }
}
