﻿using ChessGame.Common;
using ChessGame.BL.Piece;

namespace ChessGame.BL.Interfaces
{
    public interface IChessBoard
    {
        /// <summary>
        /// Adds a Piece to the chess board if the location is empty.
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="position"></param>
        /// <returns>Returns true if the piece was added to the board.</returns>
        bool AddPiece(IPiece piece, Position position);

        /// <summary>
        /// Removes a Piece from the chess board.
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="position"></param>
        /// <returns>Returns true if the piece was removed.</returns>
        bool RemovePiece(Position position);

        /// <summary>
        /// Returns if a specific position is occupied by a piece.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        bool IsEmptyPosition(Position position);

        /// <summary>
        /// Returns the Board Cell from specific position.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        IBoardCell GetBoardCell(Position position);

        /// <summary>
        /// Returns the board size.
        /// </summary>
        /// <returns></returns>
        int Size();
    }
}
