﻿namespace ChessGame.BL.Interfaces
{
    public interface IChessBoardProvider
    {
        IChessBoard GetChessBoard();
    }
}
