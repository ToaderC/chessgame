﻿using ChessGame.Common;

namespace ChessGame.BL.Interfaces
{
    public interface IBoardStorage
    {
        IBoardCell Get(Position position);
        void Set(IBoardCell cell, Position position);
        int GetSize();
    }
}
