﻿using ChessGame.BL.Piece;

namespace ChessGame.BL.Interfaces
{
    public interface IBoardCell
    {
        public IPiece? Piece { get; set; }
        bool HasPiece();
    }
}
