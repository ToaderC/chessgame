﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGame.Common
{
    // TODO: Maybe rename the class to BoardPositionChecker or Helper
    public static class BoardPosition
    {
        //public static string PositionToString(Position pos)
        //{
        //    if(pos.X < 0)
        //    {
        //        throw new ArgumentOutOfRangeException($"{nameof(pos)} out of bounds.");
        //    }

        //    //var chr = 
        //    return null;

        //}

        //public static Position StringToPosition(string pos)
        //{
        //    return new Position(0, 0);
        //}

        ///// <summary>
        ///// Used to represent board cells with chars
        ///// </summary>
        ///// <param name="nr"></param>
        ///// <returns></returns>
        ///// <exception cref="ArgumentOutOfRangeException"></exception>
        //public static char NumberToChar(int nr)
        //{
        //    if(nr < 1)
        //    {
        //        throw new ArgumentOutOfRangeException($"{nameof(nr)} out of bounds.");
        //    }

        //    return Convert.ToChar(nr + Convert.ToInt32('a') - 1);
        //}

        //public static int CharToNumber(char chr)
        //{
        //    return Convert.ToInt32(chr);
        //}

        public static bool IsIndexInBoardBounds(int index, int size)
        {
            return index >= 0 && index <= size;
        }
        public static bool IsPositionInBoardBounds(Position position, int boardSize)
        {
            if (!BoardPosition.IsIndexInBoardBounds(position.X, boardSize) || !BoardPosition.IsIndexInBoardBounds(position.Y, boardSize))
            {
                return false;
            }

            return true;
        }
    }
}
