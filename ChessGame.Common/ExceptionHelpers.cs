﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGame.Common
{
    public static class ExceptionHelpers
    {
        // TODO: might not even have to check for null because Position would not be nullable type
        public static void ThrowIfNullPosition(Position? position)
        {
            if (position == null)
            {
                throw new NullReferenceException($"{typeof(Position)} is null.");
            }
        }

        public static void ThrowIfNull<T>(T obj)
        {
            if(obj == null)
            {
                throw new NullReferenceException($"{typeof(T)} is null.");
            }
        }
    }
}
