﻿using ChessGame.Common;
using NUnit.Framework;

namespace ChessGame.CommonTests
{
    public class BoardPositionTests
    {
        [Test]
        public void IsIndexInBoardBounds_WhenInBounds_ReturnsTrue()
        {
            // Arrange
            var boardSize = 8;
            var index1 = 0;
            var index2 = boardSize;

            // Act
            var actual1 = BoardPosition.IsIndexInBoardBounds(index1, boardSize);
            var actual2 = BoardPosition.IsIndexInBoardBounds(index2, boardSize);

            // Assert
            Assert.IsTrue(actual1);
            Assert.IsTrue(actual2);
        }

        [Test]
        public void IsIndexInBoardBounds_WhenNotInBounds_ReturnsFalse()
        {
            // Arrange
            var boardSize = 8;
            var index1 = -1;
            var index2 = boardSize + 1;

            // Act
            var actual1 = BoardPosition.IsIndexInBoardBounds(index1, boardSize);
            var actual2 = BoardPosition.IsIndexInBoardBounds(index2, boardSize);

            // Assert
            Assert.IsFalse(actual1);
            Assert.IsFalse(actual2);
        }
    }
}
