﻿using ChessGame.Common;
using NUnit.Framework;
using System;

namespace ChessGame.CommonTests
{
    public class ExceptionHelpersTests
    {
        [Test]
        public void ThrowNullIfPosition_WhenNull_Throws()
        {
            // Assert
            Assert.Throws<NullReferenceException>(() => ExceptionHelpers.ThrowIfNullPosition(null));
        }

        [Test]
        public void ThrowIfNullPosition_WhenNotNull_DoesNotThrow()
        {
            // Assert
            Assert.DoesNotThrow(() => ExceptionHelpers.ThrowIfNullPosition(new Position(0, 0)));
        }
    }
}
