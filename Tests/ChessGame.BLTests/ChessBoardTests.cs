﻿using ChessGame.BL.Interfaces;
using ChessGame.Common;
using ChessGame.BL.Piece;
using Moq;
using NUnit.Framework;

namespace ChessGame.BLTests
{
    public class ChessBoardTests
    {
        private Mock<IBoardStorage> _boardStorageMock = new Mock<IBoardStorage>();
        private Mock<IBoardCell> _boardCellMock = new Mock<IBoardCell>();
        private Mock<IPiece> _pieceMock = new Mock<IPiece>();
        private ChessBoard _board;

        public ChessBoardTests()
        {
            _board = new ChessBoard(_boardStorageMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void AddPiece_WhenPositionIsNotOccupied_ReturnsTrue()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(new BoardCell());

            var result = _board.AddPiece(_pieceMock.Object, new Position(0, 0));

            Assert.True(result);
        }

        [Test]
        public void AddPiece_WhenPositionIsOccupied_ReturnsFalse()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(new BoardCell(_pieceMock.Object));

            var result = _board.AddPiece(_pieceMock.Object, new Position(0, 0));

            Assert.False(result);
        }

        [Test]
        public void RemovePiece_WhenPositionHasPiece_ReturnsTrue()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(new BoardCell(_pieceMock.Object));

            var result = _board.RemovePiece(new Position(0, 0));

            Assert.IsTrue(result);
        }

        [Test]
        public void RemovePiece_WhenPositionDoesNotHavePiece_ReturnsFalse()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(new BoardCell());

            var result = _board.RemovePiece(new Position(0, 0));

            Assert.IsFalse(result);
        }

        [Test]
        public void IsEmptyPosition_WhenNotEmpty_ReturnsTrue()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(new BoardCell(_pieceMock.Object));

            var result = _board.IsEmptyPosition(new Position(0, 0));

            Assert.IsTrue(result);
        }

        [Test]
        public void IsEmptyPosition_WhenNotEmpty_ReturnsFalse()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(new BoardCell());

            var result = _board.IsEmptyPosition(new Position(0, 0));

            Assert.IsFalse(result);
        }

        [Test]
        public void GetBoardCell_ReturnsBoardCell()
        {
            _boardStorageMock
                .Setup(x => x.Get(It.IsAny<Position>()))
                .Returns(_boardCellMock.Object);

            var result = _board.GetBoardCell(new Position(0, 0));

            Assert.IsNotNull(result);
            Assert.AreEqual(_boardCellMock.Object, result);
        }
    }
}