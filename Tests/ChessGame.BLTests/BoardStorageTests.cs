﻿using ChessGame.BL.Exceptions;
using ChessGame.BL.Interfaces;
using ChessGame.Common;
using Moq;
using NUnit.Framework;

namespace ChessGame.BLTests
{
    public class BoardStorageTests
    {
        private const int BoardSize = 8;
        private BoardStorage _boardStorage = new BoardStorage(BoardSize);
        private Mock<IBoardCell> _boardCellMock = new Mock<IBoardCell>();

        [Test]
        public void Get_WhenInvalidPosition_Throws()
        {
            // Assert
            Assert.Throws<PositionOutOfBoardException>(() => _boardStorage.Get(new Position(-1, 9)));
        }

        [Test]
        public void Get_WhenBoardCellAtPositionIsNotInitialized_Throw()
        {
            // Assert
            Assert.Throws<NullChessComponentException>(() => _boardStorage.Get(new Position(0, 0)));
        }

        // TODO: Review this test after BoardCell is implemented to check that the same BoardCell is returned
        [Test]
        public void GetAndSet_WhenValidPosition_AddsAndReturnsTheBoardCell()
        {
            // Arrange
            IBoardCell expected = new BoardCell();
            Position position = new Position(0, 0);
            _boardStorage.Set(expected, position);

            // Act
            IBoardCell actual = _boardStorage.Get(position);

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Set_WhenInvalidPosition_Throws()
        {
            // Assert
            Assert.Throws<PositionOutOfBoardException>(() => _boardStorage.Set(new BoardCell(), new Position(-1, 9)));
        }

        [Test]
        public void GetSize_WhenStorageInitialized_ReturnsSize()
        {
            // Assert
            Assert.AreEqual(BoardSize, _boardStorage.GetSize());
        }
    }
}
