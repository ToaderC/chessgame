﻿using ChessGame.BL.Piece;
using Moq;
using NUnit.Framework;

namespace ChessGame.BLTests
{
    public class BoardCellTests
    {
        private Mock<IPiece> _pieceMock = new Mock<IPiece>();

        [Test]
        public void HasPiece_WhenHasPiece_ReturnsTrue()
        {
            // Arrange
            BoardCell cell = new BoardCell(_pieceMock.Object);

            // Act
            var result = cell.HasPiece();

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void HasPiece_WhenNoPiece_ReturnsFalse()
        {
            // Arrange
            BoardCell cell = new BoardCell();

            // Act
            var result = cell.HasPiece();

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void Piece_WhenSetObject_GetReturnsObject()
        {
            // Arrange
            BoardCell cell = new BoardCell();
            cell.Piece = _pieceMock.Object;

            // Act
            var actual = cell.Piece;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(cell.Piece, actual);
        }

        [Test]
        public void Piece_WhenSetNull_GetReturnsNull()
        {
            // Arrange
            BoardCell cell = new BoardCell();
            cell.Piece = null;

            // Act
            var result = cell.Piece;

            // Assert
            Assert.IsNull(result);
        }

    }
}
